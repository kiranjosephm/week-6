import java.util.ArrayList;

public class ArrayListExamples {

	public static void main(String[] args) {
		// 1. Create a new List
		ArrayList<String> names = new ArrayList<String>();
		
		// 1. Add 2 people to the list
		names.add("kiran");
		names.add("febin");
		
		// 2. Output total number of people in list
		System.out.println("total number of elements");
		System.out.println(names.size());
		System.out.println("_____________________");
		
		
		// 3. Delete someone from the list
		names.remove(0);
		System.out.println("total number of elements after deletion");
		System.out.println(names.size());
		System.out.println("_____________________");
		
		// 4. Output all people in the list
		names.add("kiran");
		names.add("amritha");
		names.add("bhajan");
		
		//option 1
		System.out.println(names.toString());
		System.out.println("_____________________");
		
		//option 2
		for(int i=0;i<names.size();i++)
			System.out.println(names.get(i));
		System.out.println("_____________________");
		
		
		// 5. Get one person out of the list
		System.out.println(names.get(2));
		System.out.println("_____________________");
		
		// 6. Change the name of the person
		names.set(0,"jenelle");
		System.out.println(names.toString());
		System.out.println("_____________________");
		
		// 8. Loop through every item in the list and output:
				// HELLO _______ (where ___ is the name)
				for(int i=0;i<names.size();i++)
					System.out.println("hello "+names.get(i));
				System.out.println("_____________________");
		
		// 7. Delete everyone from list
		
		names.clear();
		System.out.println("total number of elements after deletion : "+names.size());
		
		
		
	}

}
