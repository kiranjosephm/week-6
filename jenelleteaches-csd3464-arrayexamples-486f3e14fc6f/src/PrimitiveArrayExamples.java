
public class PrimitiveArrayExamples {

	public static void main(String[] args) {
		// 1. Create an empty array of String
		// By default, array stores 5 names
		String[] names = new String[5];
		
		// 2. Show the total number of items in array
		System.out.println("Total number of items in array: ");
		// @TODO: Write code to output total num items to array
		System.out.println(names.length);
		
		// 3. Add something to array
		names[0] = "Jenelle";
		// @TODO: Write code to add someone to position 3
		names[2]="kiran";
		
		// 4. Output the item in position 0
		System.out.println("Name in pos 0:");
		// @TODO: Write the code to output the the name in pos 0
		System.out.println(names[0]);
		
		
		// 5. Output everything in array
		System.out.println("Everything in array: ");
		// @TODO: Write code to output everything
		for(int i=0;i<names.length;i++)
			System.out.println(names[i]);
				
		// 6. Add something to the end of the array
		//adding try catch code 
		try{
			names[7] = "Emad";
			// @TODO:  Write code to output names[7] to screen
			System.out.println(names[7]);
		}
		catch (Exception e)
		{
			System.out.println("error adding emad to array");
			//to see the error message
			System.out.println(e.toString());
		}
		
		// 7. Remove someone from the array
		// @TODO: Fill in code here
		System.out.println("Removing from the array");
		names[0]=null;
		//printing names
		for(int i=0;i<names.length;i++)
			System.out.println(names[i]);
		
		
		
		// 8. Loop through every item in the array and output:
		// HELLO _______ (where ___ is the name)
		names[0]="aaaaa";
		names[1]="bbbbbb";
		names[3]="ccccccc";
		names[4]="ddddd";
		for(int i=0;i<names.length;i++)
			System.out.println("hello "+names[i]);
		System.out.println("-----------------");
		// 9. Shift everyone by 1 position
		// @TODO: Fill in code here
		//null ,aaaaa,bbbbbb,kiran,ccccccc
		for(int i=names.length-1;i>0;i--)
			names[i]=names[i-1];
		names[0]=null;
		for(int i=0;i<names.length;i++)
			System.out.println(names[i]);
		System.out.println("-----------------");
		for(int i=1;i<names.length-1;i++)
		{
			
			String temp=names[i+1];
			names[i+1]=names[i];
			
			
			
		}
		names[0]=null;
		for(int i=0;i<names.length;i++)
			System.out.println(names[i]);
		
		
		
	}
}
